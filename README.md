# README #

### Oberon Compiler ###

* Oberon Compiler, work in progress. (In C)
* Version 1.1

### How do I get set up? ###

* Requires Linux installed.
* Requires GNU C compiler (gcc).
* Requires Maven 3+ installed.
* Install Eclipse IDE.
* Install CDT Eclipse plugin.
* Import project into workspace as existing Maven project.
* Build using 'mvn clean native:compile native:link'

### Who do I talk to? ###

* @javanerd