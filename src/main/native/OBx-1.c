/*
 ============================================================================================
 Name        : OBx-1.c
 Author      : 000235498 - Fred Laderoute
 Version     : 1.0
 Description : OBERON compiler in C, Ansi-style
 =============================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <setjmp.h>
#undef sqrt
#undef strlen
#undef accept
/****************************** Declarations **************************************************/

const char*  header = "OBERONx version 1.0";
typedef enum {
	NRESWRD       =  34 ,            /* number of reserved words                              */
    RESWRDLEN     =   9 ,            /* fixed length of a reserved word                       */
    IDBUFFSIZE    =  16 ,            /* number of significant characters in identifiers       */
    INBUFFSIZE    = 256 ,            /* input line length                                     */
    ERRMAX        = 256 ,            /* maximum error number                                  */
    SYNTAXERRNUM  =   0 ,            /* error number for syntactic error                      */
    RESWRDTABSIZE = 150 ,            /* the size of the reserved word table                   */
    SPSYMLEN      = 128 ,            /* the size of the special symbols array                 */
    STRBUFFLEN    =  80 ,            /* the size of the string buffer                         */
    HEXSTRBUFFLEN = 160 ,            /* the size of the hex string buffer                     */
    NL            = '\n',            /* newline character                                     */
    TAB           = '\t',            /* tab character                                         */
    CR            = '\r',            /* carriage return                                       */
    SPACE         = ' ' ,            /* space character                                       */
    EOLN          = '\0',            /* string terminator                                     */
    ERRMSG_SIZE   =   32             /* the size of each error message                        */
} Constant;
int*         errnums;
char*        infilenm;               /* current input filename                                */
FILE*        fp;                     /* pointer to the current input file                     */
typedef enum { false, true } bool;   /* bool type used for boolean operations                 */

const int digsmax = 8;
const int intmax = 32767;
const int exponmax = 35;
const int exponmin = -35;

/*
 * Used in solving elif ambiguity.
 */
const jmp_buf elif_jmp;

int ndigs;
int base;
int expon;

typedef enum {
    null,loop,
	with,exitsym,
	hexcharconst,
    hexstrconst,
    abssym,ident,
    intconst,realconst,
    charconst,strconst,
    chr,flt,
    lsl,ord,
    shortsym,asr,
    copy,inc,
    longsym,pack,
    unpk,assert,
    dec,incl,
    longreal,real,
    boolean,excl,
    integer,new,
    ror,charsym,
    floor,len,
    odd,set,
    plus,minus,
    times,slash,
    tilde,andsym,
    per,comma,
    semic,vertbar,
    lpar,lbrak,
    lbrace,assign,
    uparrow,eq,
    noteqsym,lt,
    gt,lte,
    gte,dotdot,
    colon,rpar,
    rbrak,rbrace,
    array,begin,
    by,casesym,
    constsym,divsym,
    dosym,elsesym,
    elsif,end,
    falsesym,forsym,
    ifsym,import,
    in,is,
    mod,module,
    nil,of,
    or,pointer,
    procedure,record,
    repeat,returnsym,
    then,to,
    truesym,type,
    until,var,
    whilesym,numsyms                  /* Number of symbols in the enum                        */
} Symbol;

char        ch;                       /* last character read from source program              */
Symbol      sym;                      /* last token read by nextsym                           */
char        idbuff[IDBUFFSIZE];       /* last identifier read by nextsym                      */
char        strbuff[STRBUFFLEN];      /* last string read                                     */
char        hexstrbuff[HEXSTRBUFFLEN];/* string buffer for hex notation strings               */
int         idlen;                    /* length of last identifer                             */
int         stringlen;                /* length of last string                                */
int         linelen;                  /* length of current line                               */
int         linenr;                   /* number of the current line                           */
int         intval;                   /* last integer read by nextsym                         */
double      realval;                  /* last real read by nextsym                            */
char        inbuff[INBUFFSIZE];       /* current line                                         */
int         inptr;                    /* pointer to current character                         */
int         symptr;                   /* pointer to beginning of current symbol               */
char*       reswrd[RESWRDTABSIZE];    /* reserved word table                                  */
Symbol      reswrdsym[RESWRDTABSIZE];
char*       symname[numsyms];         /* for debugging                                        */
Symbol      spsym[SPSYMLEN];          /* special symbols                                      */
const char* errmsg[ERRMAX];           /* error message array                                  */
char        reswrdbuff[RESWRDLEN];    /* the reserved word buffer used for hash lookup        */

char        separators[4] = { NL, TAB, CR, SPACE };
char*       syntax_msg = "unknown error";
int         rec_cmt_ctr;              /* recursive comment counter                            */
int         hexstrlen= 0;             /* string length of string represented in hex           */
bool        list;                     /* if set then print each character read from source    */
bool        list_cmts;                /* list comments                                        */
bool        prntsyms;                 /* if set then print symbols while scanning             */
bool        prevper;                  /* period already seen in last call to scanner          */
bool        spstrflag = false;        /* String flag for hex notation strings                 */
int         erroffset = 100;           /* Offset for parse errors in errmsg array              */
/****************************** Declarations **************************************************/

/****************************** Function Declarations *****************************************/
/* Scanner functions */
void get_line(void); /* get the line of the source file                                       */
void scanstr(void); /* scan a string                                                          */
char nextchar(void); /* read the next char from inbuff                                        */
void get_file_pointer(char* filename); /* get a file pointer to the file to compile           */
bool isDigit(char c); /* returns 1 if the character is a digit                                */
bool isAlpha(char c); /* returns 1 if the character is an letter                              */
bool isHex(char c); /* returns 1 if the character is a hex digit (capital case)               */
void skipsep(void); /* continue reading line until a symbol is found                          */
void print_error(int errnum); /* Print error to the console and exit                          */
Symbol nextsym(void); /* search for the next symbol                                           */
void scanident(void); /* scan next identifier                                                 */
void scannum(void); /* scan next number                                                       */
void bldident(void); /* build up an identifier                                                */
void initscalars(void); /* init scalar values                                                 */
void initsets(void); /* init sets                                                             */
void initreswrds(void); /* init reserved words                                                */
void initreswrdsyms(void); /* init reserved word symbols                                      */
void initspsyms(void); /* init special symbols                                                */
void initsymnames(void); /* init symbol names                                                 */
void initerrmsgs(void); /* init error messages                                                */
void initcompile(void); /* init compile procedure                                             */
int abs(int n); /* calc the absolute value of an int                                          */
int htoi(char c);/* convert a hexidecimal (base 16) int into a integer value (base 10)        */
/* End scanner functions */

/* Symbol Table functions */
static unsigned int hash (register const char *str, register unsigned int len);
const char * in_word_set (register const char *str, register unsigned int len);
/* End Symbol Table functions */

/* Parse Functions */
bool Statement(void);
bool StatementSequence(void);
bool FieldList(void);
bool Type(void);
bool Receiver(void);
bool FPSection(void);
bool FormalParams(void);
bool ForwardDeclaration(void);
bool ProcedureDeclaration(void);
bool ConstDeclaration(void);
bool VarDeclaration(void);
bool TypeDeclaration(void);
bool DeclarationSequence(void);
bool ImportList(void);
bool Module(void);

bool parse_error(int errnum);
bool accept(Symbol s); /* test if sym is s                                                    */
bool expect(Symbol s); /* expect that sym is s and print error if it is not                   */
/* End Parse Functions */

/****************************** Function Declarations End *****************************************/


/****************************** Scanner Begin *****************************************************/

bool accept(Symbol s) {
	if (sym == s) {
		nextsym();
		return true;
	} else {
		return false;
	}
}

bool expect(Symbol s) {
	if (accept(s))
		return true;
	else {
		return parse_error(s);
		//TODO rewrite parse error table to match each symbol id.
		//TODO So that the position in the array at id
		//TODO matches the error... 'Error X expected.' where
		//TODO X is the expected symbol.
	}

}

void initscalars() {
    linelen  = 0;
    linenr   = 0;
    inptr    = 0;
    idlen    = IDBUFFSIZE;
    ch       = ' ';
    rec_cmt_ctr = 0;
    list     = true;
    list_cmts= true;
    prntsyms = true;
    prevper  = false;
}

void initsets() {
    errnums = (int*) malloc(sizeof(int)*ERRMAX);
    int i;
    for(i = 0; i < ERRMAX; ++i) {
    	if (i < sizeof(errnums) / sizeof (int) && i >= 0) {
            errnums[i] = i+1;
    	}
    }
    for(i = 0; i < IDBUFFSIZE; ++i) {
    	if(i < sizeof(idbuff) / sizeof (char) && i >= 0) {
            idbuff[i] = SPACE;
    	}
    }
    /* Fill hexstrbuff and strbuff with spaces */
    for(i = 0; i < HEXSTRBUFFLEN; ++i)
        if(i < STRBUFFLEN && i < sizeof(strbuff) / sizeof (int) && i >= 0)
            strbuff[i] = SPACE;
         hexstrbuff[i] = SPACE;

}

void initreswrds() {
    int i;
    for( i=0; i < NRESWRD; ++i ) {
    	if(i < sizeof(reswrd) / sizeof (int) && i >= 0) {
            reswrd[i] = "";
    	}
    }

    reswrd[  2] = "ARRAY    ";
    reswrd[  6] = "TRUE     ";
    reswrd[ 11] = "WHILE    ";
    reswrd[ 12] = "CASE     ";
    reswrd[ 15] = "TO       ";
    reswrd[ 16] = "TYPE     ";
    reswrd[ 20] = "DO       ";
    reswrd[ 21] = "FOR      ";
    reswrd[ 25] = "RETURN   ";
    reswrd[ 26] = "VAR      ";
    reswrd[ 27] = "FALSE    ";
    reswrd[ 30] = "REPEAT   ";
    reswrd[ 31] = "THEN     ";
    reswrd[ 35] = "RECORD   ";
    reswrd[ 36] = "CONST    ";
    reswrd[ 40] = "MODULE   ";
    reswrd[ 41] = "OR       ";
    reswrd[ 45] = "MOD      ";
    reswrd[ 46] = "BY       ";
    reswrd[ 50] = "IS       ";
    reswrd[ 55] = "ELSE     ";
    reswrd[ 56] = "IMPORT   ";
    reswrd[ 60] = "IN       ";
    reswrd[ 61] = "WITH     ";
    reswrd[ 65] = "DIV      ";
    reswrd[ 66] = "LOOP     ";
    reswrd[ 70] = "NIL      ";
    reswrd[ 75] = "IF       ";
    reswrd[ 80] = "END      ";
    reswrd[ 85] = "OF       ";
    reswrd[ 86] = "PROCEDURE";
    reswrd[ 90] = "BEGIN    ";
    reswrd[ 95] = "POINTER  ";
    reswrd[100] = "UNTIL    ";
    reswrd[105] = "ELSIF    ";
    reswrd[110] = "EXIT     ";

}

void initreswrdsyms() {
    int i;
    for( i=0; i < RESWRDTABSIZE; ++i ) {
    	if(i < sizeof(reswrdsym) / sizeof(Symbol) && i >= 0) {
            reswrdsym[i] = null;
    	}
    }

    reswrdsym[  2] = array;
    reswrdsym[  6] = truesym;
    reswrdsym[ 11] = whilesym;
    reswrdsym[ 12] = casesym;
    reswrdsym[ 15] = to;
    reswrdsym[ 16] = type;
    reswrdsym[ 20] = dosym;
    reswrdsym[ 21] = forsym;
    reswrdsym[ 25] = returnsym;
    reswrdsym[ 26] = var;
    reswrdsym[ 27] = falsesym;
    reswrdsym[ 30] = repeat;
    reswrdsym[ 31] = then;
    reswrdsym[ 35] = record;
    reswrdsym[ 36] = constsym;
    reswrdsym[ 40] = module;
    reswrdsym[ 41] = or;
    reswrdsym[ 45] = mod;
    reswrdsym[ 46] = by;
    reswrdsym[ 50] = is;
    reswrdsym[ 55] = elsesym;
    reswrdsym[ 56] = import;
    reswrdsym[ 60] = in;
    reswrdsym[ 61] = with;
    reswrdsym[ 65] = divsym;
    reswrdsym[ 66] = loop;
    reswrdsym[ 70] = nil;
    reswrdsym[ 75] = ifsym;
    reswrdsym[ 80] = end;
    reswrdsym[ 85] = of;
    reswrdsym[ 86] = procedure;
    reswrdsym[ 90] = begin;
    reswrdsym[ 95] = pointer;
    reswrdsym[100] = until;
    reswrdsym[105] = elsif;
    reswrdsym[110] = exitsym;
}

void initspsyms() {
    char ch;
    for( ch = 0; ch < SPSYMLEN-1; ch++) {
    	if(ch > sizeof(spsym) / sizeof(char) && ch >= 0) {
            spsym[ (int)ch] = 0;
    	}
    }
    spsym[ '+' ] = plus;
    spsym[ '-' ] = minus;
    spsym[ '*' ] = times;
    spsym[ '/' ] = slash;
    spsym[ '(' ] = lpar;
    spsym[ ')' ] = rpar;
    spsym[ '[' ] = lbrak;
    spsym[ ']' ] = rbrak;
    spsym[ '=' ] = eq;
    spsym[ '<' ] = lt;
    spsym[ '>' ] = gt;
    spsym[ ',' ] = comma;
    spsym[ ';' ] = semic;
    spsym[ '.' ] = per;
    spsym[ '^' ] = uparrow;
    spsym[ '~' ] = tilde;
    spsym[ '#' ] = noteqsym;
    spsym[ '&' ] = andsym;
    spsym[ ':' ] = colon;
    spsym[ '{' ] = lbrace;
    spsym[ '}' ] = rbrace;
    spsym[ '|' ] = vertbar;
}

void initsymnames() {
    symname[ null]       = "null     ";
    symname[ loop]       = "loop     ";
    symname[ with]       = "with     ";
    symname[ exitsym]    = "exit     ";
    symname[ abssym]     = "abs      ";
    symname[ ident]      = "ident    ";
    symname[hexcharconst]= "hexconst ";
    symname[hexstrconst] = "hexstr   ";
    symname[ intconst]   = "intconst ";
    symname[ charconst]  = "charconst";
    symname[ realconst]  = "realconst";
    symname[ strconst]   = "strconst ";
    symname[ chr]        = "chr      ";
    symname[ flt]        = "flt      ";
    symname[ lsl]        = "lsl      ";
    symname[ ord]        = "ord      ";
    symname[ shortsym]   = "shortsym ";
    symname[ asr]        = "asr      ";
    symname[ copy]       = "copy     ";
    symname[ inc]        = "inc      ";
    symname[ longsym]    = "longsym  ";
    symname[ pack]       = "pack     ";
    symname[ unpk]       = "unpk     ";
    symname[ assert]     = "assert   ";
    symname[ dec]        = "dec      ";
    symname[ incl]       = "incl     ";
    symname[ longreal]   = "longreal ";
    symname[ real]       = "real     ";
    symname[ boolean]    = "boolean  ";
    symname[ excl]       = "excl     ";
    symname[ integer]    = "integer  ";
    symname[ new]        = "new      ";
    symname[ ror]        = "ror      ";
    symname[ charsym]    = "charsym  ";
    symname[ floor]      = "floor    ";
    symname[ len]        = "len      ";
    symname[ odd]        = "odd      ";
    symname[ set]        = "set      ";
    symname[ plus]       = "plus     ";
    symname[ minus]      = "minus    ";
    symname[ times]      = "times    ";
    symname[ slash]      = "slash    ";
    symname[ tilde]      = "tilde    ";
    symname[ andsym]     = "andsym   ";
    symname[ per]        = "per      ";
    symname[ comma]      = "comma    ";
    symname[ semic]      = "semic    ";
    symname[ vertbar]    = "vertbar  ";
    symname[ lpar]       = "lpar     ";
    symname[ lbrak]      = "lbrak    ";
    symname[ lbrace]     = "lbrace   ";
    symname[ assign]     = "assign   ";
    symname[ uparrow]    = "uparrow  ";
    symname[ eq]         = "eq       ";
    symname[ noteqsym]   = "numsym   ";
    symname[ lt]         = "lt       ";
    symname[ gt]         = "gt       ";
    symname[ lte]        = "lte      ";
    symname[ gte]        = "gte      ";
    symname[ dotdot]     = "dotdot   ";
    symname[ colon]      = "colon    ";
    symname[ rpar]       = "rpar     ";
    symname[ rbrak]      = "rbrak    ";
    symname[ rbrace]     = "rbrace   ";
    symname[ array]      = "array    ";
    symname[ begin]      = "begin    ";
    symname[ by]         = "by       ";
    symname[ casesym]    = "casesym  ";
    symname[ constsym]   = "constsym ";
    symname[ divsym]     = "div      ";
    symname[ dosym]      = "dosym    ";
    symname[ elsesym]    = "elsesym  ";
    symname[ elsif]      = "elsif    ";
    symname[ end]        = "end      ";
    symname[ falsesym]   = "falsesym ";
    symname[ forsym]     = "forsym   ";
    symname[ ifsym]      = "ifsym    ";
    symname[ import]     = "import   ";
    symname[ in]         = "in       ";
    symname[ is]         = "is       ";
    symname[ mod]        = "mod      ";
    symname[ module]     = "module   ";
    symname[ nil]        = "nil      ";
    symname[ of]         = "of       ";
    symname[ or]         = "or       ";
    symname[ pointer]    = "pointer  ";
    symname[ procedure]  = "procedure";
    symname[ record]     = "record   ";
    symname[ repeat]     = "repeat   ";
    symname[ returnsym]  = "returnsym";
    symname[ then]       = "then     ";
    symname[ to]         = "to       ";
    symname[ truesym]    = "truesym  ";
    symname[ type]       = "type     ";
    symname[ until]      = "until    ";
    symname[ var]        = "var      ";
    symname[ whilesym]   = "whilesym ";
}

void initerrmsgs() {
    int i;
    errmsg[  0] = "Syntax error:                  ";
    errmsg[  1] = "Incorrect number of arguments. ";
    errmsg[  2] = "Can't open file.               ";
    errmsg[  3] = "Odd number of hex digits.      ";
    errmsg[ 30] = "Integer overflow               ";
    errmsg[ 39] = "digit expected                 ";
    errmsg[ 50] = "String delimiter missing       ";
    errmsg[ 98] = "Base out of range [2..36]      ";
    errmsg[ 99] = "Digit beyond (radix-1)         ";
    /* parse errmsgs                              */
    // Fill errmsgs at 100+i and onward with
    // "symname[i] symbol expected." strings.
    // so that calling errmsg[sym] gives the
    // appropriate expected error response for that symbol.
    char* symnamebuffer = "         ";
    char symexp[23] = "symbol expected.       ";
    for (i = 0; i < numsyms; ++i) {
	    strcpy(symnamebuffer, symname[i]);
		errmsg[100+i] = symnamebuffer;
		strcat(errmsg[100+i], symexp);
		symnamebuffer = "         ";
    }
}

void initcompile() {
    printf("%s\n",header);
    initscalars();
    initsets();
    initreswrds();
    initreswrdsyms();
    initspsyms();
    initsymnames();
    initerrmsgs();
}

/**
 * Free resources.
 */
void clean() {
	free(errnums);
}

char nextchar() {
    if(inptr == linelen) {
        if(feof(fp)) {
            printf("\nEnd of file encountered, program incomplete.\n");
            fclose(fp);
            exit(-1);
        }
        get_line();
        return ch;
    }
    if(inptr < sizeof(inbuff) / sizeof(char) && inptr >= 0) {
        return ch = inbuff[++inptr];
    } else {
    	return SPACE;
    }
}

bool isDigit(char c) {
    if(c <= '9' && c >= '0')
        return true;
    return false;
}

bool isAlpha(char c) {
    if((c >= 'a' && c <= 'z') ||
            (c >= 'A' && c <= 'Z'))
        return true;
    return false;
}

bool isHex(char c) {
    if((c >= 'A' && c <= 'F'))
        return true;
    return false;
}

void skipsep() {
    unsigned int i;
    for(i = 0; i < (sizeof(separators) / sizeof(separators[0])); ++i) {
        if(ch == separators[i]) {
            nextchar();
            skipsep();
        }
    }
}

void bldident() {
    int k;
    k = 0;

    do {
        if(k < IDBUFFSIZE
        		&& k < sizeof(idbuff) / sizeof(char)
				&& idlen >= 0) {
            idbuff[++k] = ch;
        }
    } while(isDigit(nextchar()) || isAlpha(ch));

    if(k >= idlen) {
        idlen = k;
    } else {
    	if(idlen == IDBUFFSIZE)
    		idlen--;
    	while (idlen != k
    			&& idlen < sizeof(idbuff) / sizeof(char)
				&& idlen >= 0) {
            idbuff[idlen--] = SPACE; // Fill remaining space with blanks
        }
    }

    int i = 0;
    while (i < idlen
    		&& i < sizeof(idbuff) / sizeof(char)
    		&& i < sizeof(reswrdbuff) / sizeof(char)
    		&& i >= 0) {
    	i++;
    	if(isAlpha(idbuff[i]) || isDigit(idbuff[i])) {
    		reswrdbuff[i-1] = idbuff[i];
    	}
    }

    while(i < RESWRDLEN
    		&& i < sizeof(reswrdbuff) / sizeof(char)
    		&& i >= 0) {
        reswrdbuff[i++] = SPACE; //Fill remaining space with blanks
    }
    idbuff[IDBUFFSIZE - 1] = EOLN; //Fill in \0 character
}

void scanident() {
    bool reswrdflag;

    bldident();
    reswrdflag = false;
    int idx = 0;
    if(in_word_set(reswrdbuff,RESWRDLEN) != NULL) {
        reswrdflag = true;
    }

    if(reswrdflag) { /* get corresponding token name */
        // Do hash table lookup for symbol and get index.
        idx = hash(reswrdbuff,RESWRDLEN);
        sym = reswrdsym[ idx];
    } else {
        sym = ident;
    }

}

void bldint() {
    int digval;
    intval = 0;
    while(ch == '0'
    		&& hexstrlen < sizeof(hexstrbuff) / sizeof(char)
			&& hexstrlen >= 0) {
        hexstrbuff[hexstrlen++] = ch;
        nextchar();
    }
    while(isDigit(ch)
    		&& hexstrlen < sizeof(hexstrbuff) / sizeof(char)
			&& hexstrlen >= 0) {
        hexstrbuff[hexstrlen++] = ch; // Could be a string represented in hex so save in hexstrbuff
        digval = (ch - '0');
        ndigs++;
        if(intval <= ((intmax - digval) / 10)) {
            intval = 10 * intval + digval;
            nextchar();
        } else {
            nextchar();
        }
    }
}

void bldreal() {
    while(isDigit(ch)) {
        ndigs++;
        expon--;
        if(ndigs <= digsmax)
            realval = 10.0 * realval + (ch - '0');
        nextchar();
    }
}

void bldexpon() {
    int sign = 1;

    if(ch == '+')
        nextchar();
    else if(ch == '-'){
        sign = -1;
        nextchar();
    }
    if(isDigit(ch))
        bldint();
    else
        print_error(39);
    expon = expon + intval * sign;
}

int abs(int n) {
    if( n < 0)
        return n*(-1);
    else
        return n;
}

float sqr(const float f) {
    return f * f;
}

void adjustscale() {
    int pwr;
    int t;
    float f;

    if(ndigs + expon > exponmax) {
        print_error(30);
    } else {
        if(ndigs + expon < exponmin) {
            realval = 0;
        } else {
            pwr = abs(expon);
              f = 1.0;
              t = 10.0;
              do {
                  while(pwr % 2 == 0) {
                      pwr /= 2;
                      t = sqr(t);
                  }
                  pwr--;
                  f = t * f;
              } while(pwr != 0);
              if(expon >= 0)
                  realval *= f;
              else
                  realval /= f;
        }
    }
}

void clearbuff(char array[], int len) {
    while(len--
    		&& len >= 0
			&& len < sizeof(array) / sizeof(char)) {
        array[len] = SPACE;
    }
}

void scannum() {
    sym = intconst;
    ndigs = 0;
    bldint();
    if(ch == 'X') {
        spstrflag = true;
        scanstr();
        clearbuff(hexstrbuff,160);
    } else if(ch == 'H') {
        nextchar();
    }

    if(isHex(ch) || isDigit(ch)) {
        if((ch == 'E' || ch == 'D') && (nextchar() == '+' || ch == '-')) {
            sym = realconst;
            realval = intval;
            nextchar();
            expon = 0;
            bldexpon();
            if(expon != 0)
                adjustscale();
        } else {
            while((isHex(ch) || isDigit(ch))
            		&& hexstrlen < sizeof(hexstrbuff) / sizeof(char)
        			&& hexstrlen >= 0) {
                hexstrbuff[hexstrlen++] = ch;
                nextchar();
            }
            spstrflag = true;
            if(hexstrlen < sizeof(hexstrbuff) / sizeof(char)
            		&& hexstrlen >= 0) {
                hexstrbuff[hexstrlen] = 'X';
            }
            nextchar();
            scanstr();
        }
    }
    if(ndigs > digsmax || intval > intmax) {
        print_error(30);
        intval = 0;
        ndigs = 0;
    }
    if( ch == '.') {
        if( nextchar() == '.')
            prevper = true;
        else {
            sym = realconst;
            realval = intval; // coerce int into real
            expon = 0;
            if(!isDigit(ch)) {
                syntax_msg = "No digit found after decimal point.";
                print_error(0);
            }
            bldreal();
            if(ch == 'E' || ch == 'D') {
                nextchar();
                bldexpon();
            }
            if(expon != 0)
                adjustscale();
        }
    }
}

void idassign() {
    sym = colon;
    if(nextchar() == '=') {
        sym = assign;
        nextchar();
    }
}

void idrelop() {
	int i = (int) ch;
	if(i < numsyms
	&& i >= 0) {
        sym = spsym[ (int)ch];
	}
    nextchar();
    switch(ch) {
    case '=' :
        if(sym == lt)
            sym = lte;
        else
            sym = gte;
        nextchar();
        break;
    case '#' :
        if(sym == gt)
            sym = noteqsym;
        break;
    }
}

void comment()
{
  rec_cmt_ctr = 1;
  char n;
  while (nextchar() != EOF) {
    if(ch=='*') {
      if((n = nextchar()) == ')') {
          rec_cmt_ctr--;
        if(rec_cmt_ctr == 0)
          break;
      } else {// Not comment step back one.
          inptr--;
          continue;
      }
    } else if (ch=='(') {
      if ((n=nextchar())=='*')
          rec_cmt_ctr++;
      else {  // Not comment step back one.
          inptr--;
          continue;
      }
    }
  }
  nextchar();
}

void writesym() {
    int i;
    char *idptr = idbuff;
    if(sym < sizeof(symname) / sizeof(symname[0]) && sym >= 0) {
        printf("%s",symname[ sym]);
    }
    switch(sym) {
        case ident: printf("%s",idptr);
            break;
        case intconst: printf("%d",intval);
            break;
        case realconst: printf("%f",realval);
            break;
        case charconst: printf("%c",strbuff[ 1]);
            break;
        case strconst:
            if(stringlen == 0)
                printf("'''null-string'''");
            else {
                for(i = 0;i < stringlen; ++i) {
                	if (i < sizeof(strbuff) / sizeof(char) && i >= 0) {
                        printf("%c",strbuff[ i]);
                	}
                }
            }
            break;
        case hexcharconst:
        case hexstrconst:
            for(i = 0;i < hexstrlen*2; ++i) {
            	if(i < sizeof(hexstrbuff) / sizeof(char) && i >= 0) {
                    printf("%c",hexstrbuff[ i]);
            	}
            }
            break;
        default:
        	break;
    }

    printf("%c",NL);
}

int htoi(char c) {
    if(c >= 97)
        c -= 32;
    int i = c / 16 - 3;
    int j = c % 16;
    int result = i*10 + j;

    if(result > 9) result--;
        return result;
}

int htoa(char c, char d) {

    int high = htoi(c) * 16;
    int low = htoi(d);
    return high+low;
}

void scanstr() {

    if(spstrflag) {
        if((hexstrlen % 2) == 1)
            print_error(3);
        int i = 0;
        char buf = 0;
        while(i < sizeof(hexstrbuff) / sizeof(char)
           && i >= 0
		   && hexstrbuff[i++] != 'X') {
            if(i % 2 != 0
            		&& i/2 < sizeof(strbuff) / sizeof(char)
					&& i/2 >= 0
					&& i < sizeof(hexstrbuff) / sizeof(char)
					&& i >= 0) {
                strbuff[i/2] = htoa(buf, hexstrbuff[i]);
            } else if( i < sizeof(hexstrbuff) / sizeof(char)
					&& i >= 0) {
                buf = hexstrbuff[i];
            }
        }
        hexstrlen /= 2;
        sym = (hexstrlen == 1) ? hexcharconst : hexstrconst;
        spstrflag = false;
    } else {
        stringlen = 0;
        do {
            do {
                strbuff[stringlen++] = nextchar();
            } while( ch != '"' && stringlen < sizeof(strbuff) / sizeof(char) && stringlen >= 0);
            nextchar();
        } while(ch == '"');
        sym = (--stringlen == 1) ? charconst : strconst;
    }
}

/**
 * Read the next symbol.
 */
Symbol nextsym() {

    bool symfnd = false;
    do {
        symfnd = true;
        skipsep();
        symptr = inptr;

        if(isAlpha(ch)) {
            scanident();
        } else if(isDigit(ch)) {
            scannum();
        } else {
        	int i = (int) ch;
            switch(ch) {
            case ':' : idassign();
                break;
            case '>' : idrelop();
                break;
            case '<' : idrelop();
                break;
            case '.' :
                if(prevper)
                    prevper = false;
                else if(nextchar() == '.') {
                    sym = dotdot;
                    nextchar();
                } else
                    sym = per;
                break;
            case '(' : /* check for comment */
                nextchar();
                if(ch != '*')
                    sym = lpar;
                else {
                    symfnd = false;
                    nextchar();
                    comment();
                }
                break;
            case '"' :
                scanstr();
                break;
            default  :
            	if(i < numsyms && i >= 0) {
                    sym = spsym[ i];
            	}
                nextchar();
                break;
            }
        }
    } while(!symfnd);

    if(prntsyms)
        writesym();
    return sym;
}

void print_error(int errnum) {

    switch(errnum) {
    case 30:
        fprintf(stderr, "ERROR: %d :%s\n",linenr, errmsg[errnum]);
        while(isDigit(nextchar()));
        return;
    }

    if(errnum == SYNTAXERRNUM) {
        fprintf(stderr, "ERROR: %d :%s%s\n",linenr,errmsg[errnum],syntax_msg);
    } else {
    	if (errnum < sizeof(errmsg) && errnum >= 0)
        fprintf(stderr, "ERROR: %d :%s\n",linenr, errmsg[errnum]);
    }
}

void get_line() { //Done
    linenr++;

    if(list) {
        printf("%-5d: ",linenr);
    }
    inptr = 0;
    while ((ch = fgetc(fp)) != NL) {
        if(ch == EOF) {
            ++inptr;
            break;
        }
        if(inptr < sizeof(inbuff) && inptr >= 0) {
            inbuff[++inptr] = ch;
        }
        if(list) {
            printf("%c",ch);
        }
    }

    if(list) {
        printf("%c",NL);
    }
    linelen = inptr;
    inptr = 0;
}

/**
 * Get the file pointer and store it in fp.
 */
void get_file_pointer(char* filename) { //Done

    if((fp = fopen(filename, "r")) == NULL) {
        print_error(2);
    }
}


bool parse_error(int errnum) {//Error location: [line#,char index of that line]
    nextsym();
    fprintf(stderr, "\nPARSE ERROR NEAR: [%d,%d]: %s\n",linenr,inptr, errmsg[errnum+erroffset]);
    return true;
}


/****************************** Scanner End **************************************************/

/****************************** Parser Begin *************************************************/
bool Statement() {
	if(Designator()) {
		return DesignatorExpr();
	}
	switch(sym) {
		case ifsym:
			if(Expr()) {
				expect(then);
			}
		case casesym:
		case whilesym:
		case repeat:
		case forsym:
		case loop:
		case with:
		case exitsym:
		case returnsym:
		default:
			break;
	}
}
//TODO Continue at Statement

bool StatementSequence() {
	bool r = false;
	while(r = Statement()) {
		if(accept(semic)) {
			continue;
		} else {
			break;
		}
	}
	return r;
}

bool FieldList() {
	if(IdentList()) {
		return expect(colon) && Type();
	}
	return true;
}

bool Type() {//Done
	if (Qualident()) {
		return true;
	} else {
		switch(sym) {
		case array:
			expect(array);
			while(ConstExpr()) {
				if(accept(comma)) {
					continue;
				} else {
					break;
				}
			}
			if(accept(of)) {
				return Type();
			} else {
				return false;
			}
		case record:
			expect(record);
			if(accept(lpar)) {
				if(Qualident()) {
					expect(rpar);
				} else {
					return false;
				}
			}
			while(FieldList()) {
				if(accept(semic)) {
					continue;
				} else {
					break;
				}
			}
			return expect(end);
		case pointer:
			return expect(pointer) && expect(to) && Type();
		case procedure:
			if(expect(procedure)) {
				FormalParams();
				return true;
			} else {
				return false;
			}
		}
	}
}

bool Receiver() {
	if(expect(lpar)) {
		accept(var);
		return expect(ident)
			&& expect(colon)
			&& expect(ident)
			&& expect(lpar);
	} else {
		return false;
	}
}

bool FPSection() {
	accept(var);
	while(expect(ident)) {
		if (accept(comma)) {
			continue;
		} else {
			break;
		}
	}
	return expect(colon) && Type();
}

bool FormalParams() {
    expect(lpar);
    while(FPSection()) {
    	if(accept(semic)) {
    		continue;
    	} else {
    		break;
    	}
    }
    expect(rpar);
    if(accept(colon)) {
    	return Qualident();
    } else {
    	return true;
    }
}

bool ForwardDeclaration() {
	expect(procedure);
	expect(uparrow);
	Receiver();
	if(!IdentDef()) {
		return false;
	}
	FormalParams();
	return true;
}

bool ProcedureDeclaration() {
	expect(procedure);
	Receiver();

	if(!IdentDef()) {
		return false;
	}

	FormalParams();
	expect(semic);

	if(!DeclarationSequence()) {
		return false;
	}

	if(accept(begin)) {
		StatementSequence();
	}

	if(expect(end)) {
		return expect(ident);
	} else {
		return false;
	}
}

bool ConstDeclaration() {//Done
	if(IdentDef()) {
		expect(eq);
		return ConstExpr();
	} else {
		return false;
	}
}

bool VarDeclaration() {//Done
	if(IdentList()) {
		expect(colon);
		return Type();
	} else {
		return false;
	}
}
bool TypeDeclaration() {//Done
	if(IdentDef()) {
		expect(eq);
		return Type();
	} else {
		return false;
	}
}

bool DeclarationSequence() {//Done
	switch(sym) {
		case constsym:
			while(ConstDeclaration()) {
				expect(semic);
				DeclarationSequence();
			}
			break;
		case type:
			while(TypeDeclaration()) {
				expect(semic);
				DeclarationSequence();
			}
			break;
		case var:
			while(VarDeclaration()) {
				expect(semic);
				DeclarationSequence();
			}
			break;
	}

	if(ProcedureDeclaration()) {
		return expect(semic);
	} else if(ForwardDeclaration()) {
		return expect(semic);
	} else {
		return true;
	}
}

bool ImportList() {//Done
	if(accept(import)) {
		do {
			expect(ident);
			if(sym == assign) {
				expect(ident);
			}
		} while(accept(comma));
	} else {
		return false;
	}
	return expect(semic);
}

bool Module() { //Done
	nextsym();
	expect(module);
	expect(ident);
	expect(semic);
	ImportList();
	if(DeclarationSequence()) {
		if (accept(begin)) {
			if(StatementSequence()) {
				return expect(end)
					&& expect(ident)
					&& expect(per);
			} else {
				return parse_error(1);//Invalid or missing statement sequence
			}
		}
		if (accept(end)) {
			return expect(ident) && expect(per);
		}
	} else {
		return parse_error(1);//Missing Declaration sequence.
	}
	return true;
}

/****************************** Parser End ***************************************************/

/****************************** Reswrd Table *************************************************/

/* C code produced by gperf version 3.0.4 */
/* Command-line: gperf keywords.gperf  */
/* Computed positions: -k'1-2,4' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gnu-gperf@gnu.org>."
#endif

#define TOTAL_KEYWORDS 36
#define MIN_WORD_LENGTH 9
#define MAX_WORD_LENGTH 9
#define MIN_HASH_VALUE 2
#define MAX_HASH_VALUE 110
/* maximum key range = 109, duplicates = 0 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
/*ARGSUSED*/
static unsigned int hash (str, len)
     register const char *str;
     register unsigned int len;
{
  static unsigned char asso_values[] =
    {
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111,   5, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111,   1,   0,  30,   6,   5,
       45,   6,   1,  55,  25, 111,   5,  21,  30,  10,
       35,  55,   1,  20,  60,   0,  15,  20,   5,  11,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111, 111, 111, 111,
      111, 111, 111, 111, 111, 111, 111
    };
  return asso_values[(unsigned char)str[3]] + asso_values[(unsigned char)str[1]] + asso_values[(unsigned char)str[0]+1];
}

#ifdef __GNUC__
__inline
#if defined __GNUC_STDC_INLINE__ || defined __GNUC_GNU_INLINE__
__attribute__ ((__gnu_inline__))
#endif
#endif


const char * in_word_set (str, len)
     register const char *str;
     register unsigned int len;
{

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register const char *s = reswrd[key];

          if (*str == *s && !strcmp (str + 1, s + 1))
            return s;
        }
    }
  return 0;
}
/***************************** Reswrd Table End ***********************************************/

int main(int argc, char *argv[]) {

#ifdef _OPENMP
    printf("Compiled by an OpenMP-compliant implementation.\n");
#endif
    initcompile();

    if(argc != 2 || (infilenm = argv[1]) == NULL) {
        print_error(1);
        exit(-1);
    }

    get_file_pointer(infilenm);
    Module();
    clean();
    return EXIT_SUCCESS;;
}
